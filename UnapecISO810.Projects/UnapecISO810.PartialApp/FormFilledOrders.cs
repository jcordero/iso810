﻿using System;
using System.Data;
using System.Linq;
using System.Windows.Forms;
using UnapecISO810.Framework.Data;

namespace UnapecISO810.PartialApp
{
    public partial class FormFilledOrders : Form
    {

        private readonly DataContext dbContext;


        public FormFilledOrders()
        {
            InitializeComponent();

            dbContext = new DataContext();
        }

        private void FormFilledOrders_Load(object sender, EventArgs e)
        {
            var list = dbContext.RefillmentOrders.Select(p => 
            new { Transaccion = p.TransactionCode, Producto = p.ProductId, Nombre = p.ProductName,
                Solicitado = p.Quantity, Despachado = p.QuantitySupplied, Fecha = p.RegisterDate,
                FechaEntrega = p.ResponseDate, Vendedor = p.SellerId }).ToList();

            dataGridView1.DataSource = list;
        }
    }
}
