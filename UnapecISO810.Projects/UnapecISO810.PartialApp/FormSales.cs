﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Data.Entity.Migrations;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

using UnapecISO810.Framework.Data;
using UnapecISO810.Framework.Services;

namespace UnapecISO810.PartialApp
{
    public partial class FormSales : Form
    {

        private readonly DataContext dbContext;
        private readonly SalesOrderService _salesOrderService;

        public FormSales()
        {
            InitializeComponent();
            dbContext = new DataContext();
            _salesOrderService = new SalesOrderService();
        }

        private void FormSales_Load(object sender, EventArgs e)
        {
            var products = dbContext.Products.ToList();

            cbProducts.DataSource = products;
            cbProducts.DisplayMember = "Name";
            //cbProducts.ValueMember = "Id";
        }

        private void btnClose_Click(object sender, EventArgs e)
        {
            this.Close();
        }

        private void btnRegister_Click(object sender, EventArgs e)
        {
            if(cbProducts.SelectedValue == null)
            {
                MessageBox.Show(this, "Debe seleccionar un producto.");
                return;
            }

            if (txtQuantity.Value <= 0)
            {
                MessageBox.Show(this, "La Cantidad de Artículos debe ser mayor que cero.");
                txtQuantity.Focus();
                return;
            }

            var product = cbProducts.SelectedValue as Product;

            var requestedQty = (int)txtQuantity.Value;
            var stock = dbContext.Inventories.Single(p => p.ProductId == product.Id).Quantity;

            if ((stock - requestedQty) < 0)
            {
                MessageBox.Show(this, 
                    string.Format("La Cantidad de productos seleccionados, excede el inventario actual. Stock Actual: {0}", stock));
                return;
            }

            var sale = new SaleOrder
            {
                Quantity = Convert.ToInt32(txtQuantity.Value),
                Amount = (decimal)(requestedQty * product.Price),
                RegisterDate = DateTimeOffset.Now,
                ProductId = product.Id
            };

            _salesOrderService.RegisterSales(sale);
          

            MessageBox.Show(this, "Acto de Venta registrado.");

            this.Close();
        }

        private void txtTotal_Validated(object sender, EventArgs e)
        {

        

            if (cbProducts.SelectedValue != null)
            {
                var product = cbProducts.SelectedValue as Product;
                if (txtQuantity.Value > 0M)
                {
                    txtTotal.Text = (Convert.ToInt32(txtQuantity.Value) * product.Price).ToString("C");
                }
                else
                {
                    txtTotal.Text = product.Cost.ToString("C");
                }
            }
            else
            {
                txtTotal.Text = "0.00";
            }
        }
    }
}
