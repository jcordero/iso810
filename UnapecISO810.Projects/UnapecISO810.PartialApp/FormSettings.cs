﻿using System;
using System.Configuration;
using System.Windows.Forms;

namespace UnapecISO810.PartialApp
{
    public partial class FormSettings : Form
    {

        private Configuration _configuration;

        public FormSettings()
        {
            InitializeComponent();
            //
            _configuration = ConfigurationManager.OpenExeConfiguration(ConfigurationUserLevel.None);
            txtPathReading.Text = _configuration.AppSettings.Settings["RefillmentPathImport"].Value;
            txtPathWriting.Text = _configuration.AppSettings.Settings["RefillmentPathExport"].Value;
        }

        private void button1_Click(object sender, EventArgs e)
        {
            _configuration.AppSettings.Settings["RefillmentPathImport"].Value = txtPathReading.Text;
            _configuration.AppSettings.Settings["RefillmentPathExport"].Value = txtPathWriting.Text;
            _configuration.Save(ConfigurationSaveMode.Modified);

            MessageBox.Show(this, "Configuraciones guardadas correctamente.");
        }

        private void button2_Click(object sender, EventArgs e)
        {
            this.Close();
        }
    }
}
