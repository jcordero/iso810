﻿using System;
using System.Data;
using System.Linq;
using System.Windows.Forms;
using UnapecISO810.Framework.Data;

namespace UnapecISO810.PartialApp
{
    public partial class FormProducts : Form
    {

        private readonly DataContext dbContext;

        public FormProducts()
        {
            InitializeComponent();

            dbContext = new DataContext();

        }

        private void FormProducts_Load(object sender, EventArgs e)
        {
            var list = dbContext.Inventories.Select(p=> new { ID = p.ProductId, Nombre = p.Product.Name, Coste = p.Product.Cost, Inventario = p.Quantity }).ToList();
            dataGridView1.DataSource = list;
        }
    }
}
