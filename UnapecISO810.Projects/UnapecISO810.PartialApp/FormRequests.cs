﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Configuration;
using System.Data;
using System.Data.Entity.Migrations;
using System.IO;
using System.Linq;
using System.Threading.Tasks;
using System.Windows.Forms;
using UnapecISO810.Framework.Data;
using UnapecISO810.Framework.Services;

namespace UnapecISO810.PartialApp
{
    public partial class FormRequests : Form
    {

        private readonly RefillmentXmlManagerService _refillmentManagerService;

        private bool canRun = false;

        public FormRequests()
        {
            InitializeComponent();
            backgroundWorker.DoWork += BackgroundWorker_DoWork;
            //
            _refillmentManagerService = new RefillmentXmlManagerService();
        }

        private void FormRequests_Load(object sender, EventArgs e)
        {
            canRun = true;
            backgroundWorker.RunWorkerAsync();
        }

        private async void BackgroundWorker_DoWork(object sender, DoWorkEventArgs e)
        {
            while (canRun)
            {
                var files = Directory.EnumerateFiles(ConfigurationManager.AppSettings["RefillmentPathImport"], "*.xml", SearchOption.TopDirectoryOnly).Where(p => !p.EndsWith(PROCESSED_FILES_EXTENSION));
                foreach (var fileName in files)
                {
                    //Open file
                    var refillmentOrders = _refillmentManagerService.GetRequest(fileName);

                    if (refillmentOrders != null && refillmentOrders.Any())
                    {
                        using (var dbContext = new DataContext())
                        {
                            dbContext.RefillmentOrders.AddRange(refillmentOrders);
                            dbContext.SecureSaveChanges();
                        }
                    }
                    
                    RelocateFile(fileName);
                }

                await Task.Delay(5 * 1000);
            }
        }

        private static void RelocateFile(string fileName)
        {
            var newFileName = string.Format("{1}{0}{2}{3}", Path.DirectorySeparatorChar, Path.GetDirectoryName(fileName), Path.GetFileNameWithoutExtension(fileName), PROCESSED_FILES_EXTENSION);
            File.Move(fileName, newFileName);
        }

        private void button1_Click(object sender, EventArgs e)
        {
            var orders = dataGridView1.DataSource as ICollection<RefillmentOrder>;

            ExtractOrdersToProcess(sender, e, orders);
        }

        private void button2_Click(object sender, EventArgs e)
        {
            var selection = new List<RefillmentOrder>();

            foreach (DataGridViewRow requestRow in dataGridView1.Rows)
            {
                if (Convert.ToBoolean(requestRow.Cells[DATA_GRIDVIEW_CHECKBOX_COLUMN].Value))
                {
                    selection.Add(requestRow.DataBoundItem as RefillmentOrder);
                }
            }

            ExtractOrdersToProcess(sender, e, selection);
        }

        private void ExtractOrdersToProcess(object sender, EventArgs e, ICollection<RefillmentOrder> orders)
        {
            var task = GenerateResponses(orders.Select(p => p.Id));
            task.ContinueWith(t =>
            {
                MessageBox.Show(this, "Solicitudes procesadas correctamente.");
                button4_Click(sender, e);
            }, TaskContinuationOptions.ExecuteSynchronously);
        }

        private void button3_Click(object sender, EventArgs e)
        {
            this.Close();
        }

        private void button4_Click(object sender, EventArgs e)
        {
            using (var dbContext = new DataContext())
            {
                dataGridView1.DataSource = dbContext.RefillmentOrders.Where(p => p.ResponseDate == null).ToList();

                dataGridView1.Columns.RemoveAt(0);
                dataGridView1.Columns.RemoveAt(3);
                foreach (var item in dataGridView1.Columns.OfType<DataGridViewColumn>())
                {
                    item.ReadOnly = true;
                }
                dataGridView1.Columns.Insert(0, GetCheckBoxColumn());

                dataGridView1.Refresh();
            }
        }

        private DataGridViewColumn GetCheckBoxColumn()
        {
            return new DataGridViewCheckBoxColumn(false) { Name = DATA_GRIDVIEW_CHECKBOX_COLUMN, Width = 25, HeaderText = "", ReadOnly = false };
        }

        private async Task GenerateResponses(IEnumerable<int> orderKeys)
        {
            await Task.Factory.StartNew(() =>
            {
                using (var dbContext = new DataContext())
                {
                    foreach (var item in orderKeys)
                    {
                        var request = dbContext.RefillmentOrders.FirstOrDefault(p => p.Id == item);
                        if (request == null) continue;

                        var inventory = dbContext.Inventories.SingleOrDefault(p => p.ProductId == request.ProductId);

                        
                        if (inventory.Quantity >= request.Quantity)
                        {
                            request.QuantitySupplied = request.Quantity;
                            inventory.Quantity -= request.Quantity;
                        }
                        else 
                        {
                            request.QuantitySupplied = inventory.Quantity;
                            inventory.Quantity = 0;
                        }

                        var content = _refillmentManagerService.CreateResponse(request);
                        CreateOutputFiles(content, request.TransactionCode);

                        request.ResponseDate = DateTimeOffset.Now;
                        dbContext.RefillmentOrders.AddOrUpdate(request);
                        dbContext.SecureSaveChanges();
                    }
                }
            });
        }

        private void CreateOutputFiles(string content, string orderId)
        {
            var outputPath = ConfigurationManager.AppSettings["RefillmentPathExport"];
            var auditPath = ConfigurationManager.AppSettings["RefillmentPathCloneCopy"];

            var fileName = string.Format("{0}_entrega_{1}_{2}.xml", SystemValues.CREATOR_GROUP_ID, orderId, DateTimeOffset.Now.ToString("yyyyMMdd"));

            File.WriteAllText(Path.Combine(outputPath, fileName), content);
            File.WriteAllText(Path.Combine(auditPath, fileName), content);
        }

        private const string DATA_GRIDVIEW_CHECKBOX_COLUMN = "chkBoxMark";
        private const string PROCESSED_FILES_EXTENSION = "_PRC.xml";

    }
}
