﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using TextFileWriterApp.Data;

namespace TextFileWriterApp
{
    public partial class FormSales : Form
    {

        public FormSales()
        {
            InitializeComponent();
        }

        private void FormSales_Load(object sender, EventArgs e)
        {
            var products = FormMain.DataService.GetProducts();

            cbProducts.DataSource = products;
            cbProducts.DisplayMember = "Name";
            //cbProducts.ValueMember = "Id";
        }

        private void btnClose_Click(object sender, EventArgs e)
        {
            this.Close();
        }

        private void btnRegister_Click(object sender, EventArgs e)
        {

            if(cbProducts.SelectedValue == null)
            {
                MessageBox.Show(this, "Debe seleccionar un producto.");
                return;
            }

            var product = cbProducts.SelectedValue as Product;

            var sale = new SaleOrder
            {
                Quantity = Convert.ToInt32(txtQuantity.Value),
                Amount = Convert.ToInt32(txtQuantity.Value) * product.Cost,
                RegisterDate = DateTimeOffset.Now,
                ProductId = product.Id
            };

            FormMain.DataService.AddSales(sale);
            MessageBox.Show(this, "Acto de Venta registrado.");

            this.Close();
        }

        private void txtTotal_Validated(object sender, EventArgs e)
        {
            if (cbProducts.SelectedValue != null)
            {
                var product = cbProducts.SelectedValue as Product;
                if (txtQuantity.Value > 0M)
                {
                    txtTotal.Text = (Convert.ToInt32(txtQuantity.Value) * product.Cost).ToString("C");
                }
                else
                {
                    txtTotal.Text = product.Cost.ToString("C");
                }
            }
            else
            {
                txtTotal.Text = "0.00";
            }


           
        }
    }
}
