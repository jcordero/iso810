﻿namespace TextFileWriterApp.Data
{
    public class Product
    {
        public string Id { get; set; }
        public string Name { get; set; }
        public decimal Cost { get; set; }
        public int Stock { get; set; }
    }
}
