﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace TextFileWriterApp.Data
{
    public class ExporterService
    {


        public static string GenerateCsvFile(IEnumerable<SaleOrder> salesOrders, IEnumerable<Product> products)
        {
            var buffer = new StringBuilder();

            foreach (var item in products)
            {
                var salesQty = salesOrders.Where(p => p.ProductId == item.Id).Sum(p => p.Quantity);

                buffer.AppendFormat("{0},{1},{2},{3},{4}", 
                                    item.Id, item.Name, item.Cost, item.Stock, salesQty);

                buffer.AppendLine();
            }


            return buffer.ToString();
        }


    }
}
