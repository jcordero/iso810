﻿using System;
using System.Collections.Generic;

namespace TextFileWriterApp.Data
{
    public class SaleOrder
    {
        public string Id { get; set; }
        public decimal Amount { get; set; }
        public DateTimeOffset RegisterDate { get; set; }
        public string ProductId { get; set; }
        public int Quantity { get; set; }
    }


}
