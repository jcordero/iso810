﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace TextFileWriterApp.Data
{
    public class DataService
    {

        private static IList<SaleOrder> _salesOrders;

        public DataService()
        {
            _salesOrders = new List<SaleOrder>();
        }

        public IEnumerable<Product> GetProducts()
        {
            return _products;
        }

        public void AddSales(SaleOrder order)
        {


            _salesOrders.Add(order);
        }

        public IEnumerable<SaleOrder> GetSalesOrders()
        {
            return _salesOrders;
        }


        private static IList<Product> _products = new List<Product>
        {
            new Product { Id = "ABC0001", Name = "Pañales Don Ramón", Cost = 24.75M, Stock = 500 },
            new Product { Id = "PDC001", Name = "Pasta Dental Colgate", Cost = 78.99M, Stock = 50 },
            new Product { Id = "PDC002", Name = "Pastal Dental Oral-B", Cost = 65.95M, Stock = 50 },
            new Product { Id = "PDC009", Name = "Cepillo Dental Dentina", Cost = 24.75M, Stock = 25 }
        };


    }
}
