﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using TextFileWriterApp.Data;

namespace TextFileWriterApp
{
    public partial class FrmTotalSales : Form
    {

        public FrmTotalSales()
        {
            InitializeComponent();
        }

        private void FrmTotalSales_Load(object sender, EventArgs e)
        {
            var data = FormMain.DataService.GetSalesOrders();
            dataGridView1.DataSource = data;
        }
    }
}
