﻿using System;
using System.Windows.Forms;
using TextFileWriterApp.Data;

namespace TextFileWriterApp
{
    public partial class FormMain : Form
    {

        internal static DataService DataService = null;

        public FormMain()
        {
            InitializeComponent();

            DataService = new DataService();
        }

        private void salirToolStripMenuItem_Click(object sender, EventArgs e)
        {
            Application.Exit();
        }

        private void exportarInventarioToolStripMenuItem_Click(object sender, EventArgs e)
        {
            var r = saveFileDialog1.ShowDialog(this);

            if (r != DialogResult.OK)
                return;

            var sales = DataService.GetSalesOrders();
            var products = DataService.GetProducts();
            var csvOutput = ExporterService.GenerateCsvFile(sales, products);

            System.IO.File.WriteAllText(saveFileDialog1.FileName, csvOutput);

            MessageBox.Show(this, "Archivo generado exitosamente.");
        }

        private void registrarVentaToolStripMenuItem_Click(object sender, EventArgs e)
        {
            var frm = new FormSales();
            frm.MdiParent = this;
            frm.Show();
        }

        private void productosToolStripMenuItem_Click(object sender, EventArgs e)
        {
            var frm = new FormProducts();
            frm.MdiParent = this;
            frm.Show();
        }

        private void totalDeVentasToolStripMenuItem_Click(object sender, EventArgs e)
        {
            var frm = new FrmTotalSales();
            frm.MdiParent = this;
            frm.Show();
        }
    }
}
