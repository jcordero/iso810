﻿App = App || {};

(function () {

    function BuyModalViewModel() {
        var self = this;

        self.units = ko.observableArray([]);
        self.products = ko.observableArray([]);

        $.getJSON('/Product/GetUnits', {}, function (registeredUnits) {
            if (registeredUnits && registeredUnits.length > 0) {
                self.units(registeredUnits);

                $.getJSON('/Product/Get', {}, function (response) {
                    if (response && response.length > 0) {
                        for (var i = 0; i <= response.length; i++) {

                            var product = {
                                IsSelected: false,
                                ProductName: response[i].Description,
                                ProductCode: response[i].Code,
                                OrderId: i,
                                ProductUnitId: 0,
                                PriceUnit: 0,
                                Quantity: 0
                            };

                            self.products.push(product);
                        }
                    }
                });
            }
        });

        self.pushIntention = function (obj) {

            var data = ko.toJS(self.products());
            $.ajax({
                type: "POST",
                url: '/Shop/Create',
                data: { items: data },
                dataType: "json",
                traditional: false,
                success: function (response) {
                    App.showMessage('success', 'Oferta.', 'Intención de Compra enviada exitosamente.');
                    setTimeout(function () { document.location = "Index"; }, 2500);
                },
                error: function (response) {
                    App.showMessage('error', 'Error en el envío', response.statusText);
                }
            });

        };

        return self;
    }


    var products = undefined;

    $(document).ready(function () {

        var vm = new BuyModalViewModel();
        ko.applyBindings(vm, document.getElementById('BuyIntentionView'));

        $('#products-group').slimscroll({
            height: 420
        });

    });


})();
