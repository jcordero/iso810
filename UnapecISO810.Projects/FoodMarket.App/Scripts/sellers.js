﻿App = App || {};

(function () {

    function SellModalViewModel() {
        var self = this;

        self.seller = ko.observable('');
        self.date = ko.observable('');
        self.products = ko.observableArray([]);

        self.loadData = function (data) {
            self.seller(data.Seller);
            self.date(moment(data.DateCreation).format('DD/MM/YYYY hh:MM:ss'));
            self.products(data.ProductList);
        };

        return self;
    }

    function BuyModalViewModel() {
        var self = this;

        self.intentions = ko.observableArray([]);
        self.selectedSellIntentionId = 0;

        self.fill = function (data) {
            $.getJSON('/Shop/GetIntentions', {}, function (response) {
                self.intentions(response);
            });
        };

        self.pushDeal = function (id) {
            if (!self.selectedSellIntentionId || self.selectedSellIntentionId < 0)
            {
                return;
            }
            
            $.post('/Sellers/MakeOffer?id=' + self.selectedSellIntentionId + '&offerId=' + id, {}, function (response) {
                App.showMessage('success', 'Oferta.', 'Oferta de compra enviada exitosamente.');
                setTimeout(function () { document.location = "Index"; }, 2500);
            });
        };

        return self;
    }


    $(document).ready(function () {

        var vm = new SellModalViewModel();
        ko.applyBindings(vm, document.getElementById('sellDetailModal'));

        $('a.triggerModal').click(function () {
            var v = this.attributes['data-sellId'].value;
            $.getJSON('/Sellers/GetIntention?id=' + v, {}, function (response) {
                vm.loadData(response);
                $('#sellDetailModal').modal();
            });
        });

        var buy_vm = new BuyModalViewModel();
        ko.applyBindings(buy_vm, document.getElementById('myOffersModal'));
        buy_vm.fill();

        $('a.doMakeDeal').click(function () {
            var v = this.attributes['data-sellId'].value;
            buy_vm.selectedSellIntentionId = v;
            $('#myOffersModal').modal();
        });

        $('#productsOffer').slimscroll({ height: 380 });
        $('#myOffersModal .modal-body').slimscroll({ height: 280 });

        $('#sellsListDatatable').DataTable({
            "paging": true,
            "ordering": true,
            "info": false,
            "searching": false,
            "language": App.ui.datatablesResources
        });
    });

})();
