﻿App = App || {};

(function () {

    function BuyModalViewModel() {
        var self = this;

        self.date = ko.observable('');
        self.products = ko.observableArray([]);

        self.loadData = function (data) {
            self.date(moment(data.DateCreation).format('DD/MM/YYYY hh:MM:ss'));
            self.products(data.ProductList);
        };

        return self;
    }


    $(document).ready(function () {

        var vm = new BuyModalViewModel();
        ko.applyBindings(vm, document.getElementById('buyDetailModal'));

        $('a.triggerModal').click(function () {
            var v = this.attributes['data-sellId'].value;
            $.getJSON('/Shop/GetIntention?id=' + v, {}, function (response) {
                vm.loadData(response);
                $('#buyDetailModal').modal();
            });
        });

        $('#buysListDatatable').DataTable({
            "paging": true,
            "ordering": true,
            "info": false,
            "searching": false,
            "language": App.ui.datatablesResources
        });

    });

})();
