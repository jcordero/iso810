using FoodMarket.App.AgroMarketService;
using FoodMarket.App.Models;
using FoodMarket.App.Services;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Web.Mvc;

namespace FoodMarket.App.Controllers
{
    public class OffersController : Controller
    {
        private readonly OfferService _offerService;

        public OffersController()
        {
            _offerService = new OfferService(System.Web.HttpContext.Current.Session.TransactionServiceToken());
        }

        public ActionResult Index()
        {
            var offers = _offerService.GetOffers();
            return View(offers);
        }

        
    }
}