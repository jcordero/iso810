using FoodMarket.App.AgroMarketService;
using FoodMarket.App.Models;
using FoodMarket.App.Services;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Web.Mvc;

namespace FoodMarket.App.Controllers
{
    public class ShopController : Controller
    {
        private readonly BuyingIntentionService _buyingService;

        public ShopController()
        {
            _buyingService = new BuyingIntentionService(System.Web.HttpContext.Current.Session.TransactionServiceToken());
        }

        public ActionResult Index()
        {
            var intentions = _buyingService.GetBuyIntentions();
            return View(intentions.Where(p => p.BuyerId == ServiceAccess.GROUP_ACCESS_ID));
        }

        public ActionResult Create()
        {
            return View();
        }

        [HttpPost]
        public ActionResult Create(IntentionBuyingProductDTO[] items)
        {
            if (items == null || !items.Any())
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest, "Listado de productos es inv�lido. Verifique y vuelva a intentar.");

            { // validations

                if (!items.Any(p => p.IsSelected))
                    return new HttpStatusCodeResult(HttpStatusCode.BadRequest, "No ha seleccionado ningun art�culo. Seleccione y vuelva a intentar.");

                var isAnyInvalid = items.FirstOrDefault(p => p.IsSelected && (p.ProductUnitId == 0 || p.PriceUnit <= 0));
                if (isAnyInvalid != null)
                    return new HttpStatusCodeResult(HttpStatusCode.BadRequest, string.Format("El producto {0} debe contener Unidad y Precio x Unidad.", isAnyInvalid.ProductName));
            }

            var products = items.Where(p => p.IsSelected).Select(p => p as ProductIntention);

            _buyingService.CreateBuying(products);
            return Json(HttpStatusCode.OK);
        }

        [HttpGet]
        public JsonResult GetIntention(int id)
        {
            var intentions = _buyingService.GetIntention(id);
            return Json(intentions, JsonRequestBehavior.AllowGet);
        }

        [HttpGet]
        public JsonResult GetIntentions()
        {
            var intentions = _buyingService.GetBuyIntentions().Where(p => p.BuyerId == ServiceAccess.GROUP_ACCESS_ID);
            return Json(intentions, JsonRequestBehavior.AllowGet);
        }

        [HttpPost]
        public JsonResult MakeOffer(int id, int offerId)
        {
            _buyingService.CreateOffer(id, offerId);
            return Json(JsonRequestBehavior.AllowGet);
        }
    }
}