using FoodMarket.App.Services;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using System.Web.Mvc;

namespace FoodMarket.App.Controllers
{
    public class SellersController : Controller
    {
        private readonly SellingIntentionService _sellingService;

        public SellersController()
        {
            _sellingService = new SellingIntentionService(System.Web.HttpContext.Current.Session.TransactionServiceToken());
        }

        public ActionResult Index()
        {
            var intentions = _sellingService.GetSellIntentions();
            return View(intentions);
        }

        [HttpGet]
        public JsonResult GetIntention(int id)
        {
            var intentions = _sellingService.GetIntention(id);
            return Json(intentions.FirstOrDefault(), JsonRequestBehavior.AllowGet);
        }

        [HttpPost]
        public JsonResult MakeOffer(int id, int offerId)
        {
             _sellingService.CreateOffer(id, offerId);
            return Json(JsonRequestBehavior.AllowGet);
        }
    }
}