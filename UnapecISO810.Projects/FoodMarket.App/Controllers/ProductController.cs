﻿using FoodMarket.App.AgroMarketService;
using FoodMarket.App.Services;
using System.Web.Mvc;

namespace FoodMarket.App.Controllers
{
    public class ProductController : Controller
    {
        private readonly ProductService _productService;

        public ProductController()
        {
            _productService = new ProductService(System.Web.HttpContext.Current.Session.TransactionServiceToken());
        }

        public ActionResult Index()
        {
            var products = _productService.GetProducts();
            return View(products);
        }

        public ActionResult Create()
        {
            return View();
        }

        [HttpGet]
        public JsonResult Get()
        {
            var products = _productService.GetProducts();
            return Json(products, JsonRequestBehavior.AllowGet);
        }

        [HttpGet]
        public JsonResult GetUnits()
        {
            var units = _productService.GetUnits();
            return Json(units, JsonRequestBehavior.AllowGet);
        }

        //[ValidateAntiForgeryToken]
        //[HttpPost]
        //public ActionResult Create(Product product)
        //{
        //    _productService.Add(product);
        //    return RedirectToAction("Index");
        //}

    }
}