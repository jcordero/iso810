﻿using Microsoft.Owin;
using Owin;

[assembly: OwinStartupAttribute(typeof(FoodMarket.App.Startup))]
namespace FoodMarket.App
{
    public partial class Startup
    {
        public void Configuration(IAppBuilder app)
        {
            ConfigureAuth(app);
        }
    }
}
