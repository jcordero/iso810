﻿using FoodMarket.App.AgroMarketService;
using System.Collections.Generic;
using System.Linq;
using System;

namespace FoodMarket.App.Services
{
    public class BuyingIntentionService : InternalService
    {

        public BuyingIntentionService(string token) : base(token)
        {

        }

        public IEnumerable<IntentionBuying> GetBuyIntentions()
        {
            var response = _marketServiceClient.GetAllIntentionsToBuy(ServiceAccess.USERNAME, Token);
            _marketServiceClient.Close();
            return response.Intentions;
        }

        public IntentionBuying GetIntention(int id)
        {
            var response = _marketServiceClient.GetIntentionToBuy(ServiceAccess.USERNAME, Token, id);
            _marketServiceClient.Close();
            return response.Intentions.FirstOrDefault();
        }

        public bool CreateOffer(int sellIntentionId, int buyIntentionId)
        {
            var response = _marketServiceClient.MakeDeal(ServiceAccess.USERNAME, Token, sellIntentionId, buyIntentionId);
            _marketServiceClient.Close();
            return true;
        }

        public int CreateBuying(IEnumerable<ProductIntention> products)
        {
            var intention = new IntentionToBuyFromProducts
            {
                token = Token,
                userName = ServiceAccess.USERNAME
            };

            var productIntentions = new List<InnerProductRequest>();

            foreach (var p in products)
            {
                var productRequest = new InnerProductRequest
                {
                    PriceUnit = p.PriceUnit,
                    ProductCode = p.ProductCode,
                    ProductUnit = p.ProductUnitId,
                    Quantity = Convert.ToInt32(p.Quantity),
                };
                productIntentions.Add(productRequest);
            }

            intention.ProductList = productIntentions;
            var response = _marketServiceClient.CreateIntentionToBuy(intention);
            _marketServiceClient.Close();
            return response.Id;
        }

    }
}