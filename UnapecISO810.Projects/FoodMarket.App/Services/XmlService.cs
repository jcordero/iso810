﻿using System;
using System.Xml.Linq;

namespace FoodMarket.App.Services
{
    public class XmlService : IDisposable
    {
        private XDocument _document;

        public void Dispose()
        {
            _document = null;
        }


        public string Serialize(object obj)
        {
            var element = ToElement(obj);

            _document = new XDocument();
            _document.Add(new XElement(MAIN_XML_ELEMENT_NAME, element));
            return _document.ToString(SaveOptions.None);
        }

        private XElement ToElement(object entity)
        {
            var item = new XElement(XML_ELEMENT_NAME,
                new XElement(IdIntencion, ""),
                new XElement(IdEmpresa, ""),
                new XElement(FechaCreacion, ""),
                new XElement(CodigoProducto, ""),
                new XElement(Cantidad, ""),
                new XElement(Unidad, ""),
                new XElement(PrecioPorUnidad, ""));
            return item;
        }


        private const string MAIN_XML_ELEMENT_NAME = "records";
        private const string XML_ELEMENT_NAME = "registry";

        private const string IdIntencion = "";
        private const string IdEmpresa = "";
        private const string FechaCreacion = "";
        private const string CodigoProducto = "";
        private const string Cantidad = "";
        private const string Unidad = "";
        private const string PrecioPorUnidad = "";

    }
}