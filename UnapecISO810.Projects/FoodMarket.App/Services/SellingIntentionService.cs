﻿using FoodMarket.App.AgroMarketService;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace FoodMarket.App.Services
{
    public class SellingIntentionService : InternalService
    {

        public SellingIntentionService(string token) : base(token)
        {

        }

        public IEnumerable<Sell> GetAllSells()
        {
            var response = _marketServiceClient.GetAllSells(ServiceAccess.USERNAME, Token);
            _marketServiceClient.Close();
            return response.SellList;
        }

        public IEnumerable<IntentionSell> GetSellIntentions()
        {
            var response = _marketServiceClient.GetIntentionsToSell(ServiceAccess.USERNAME, Token);
            _marketServiceClient.Close();
            return response.Intentions;
        }

        public IEnumerable<IntentionSell> GetIntention(int id)
        {
            var response = _marketServiceClient.GetIntentionToSell(ServiceAccess.USERNAME, Token, id);
            _marketServiceClient.Close();
            return response.Intentions;
        }

        public bool CreateOffer(int sellIntentionId, int buyIntentionId)
        {
            var response = _marketServiceClient.MakeDeal(ServiceAccess.USERNAME, Token, sellIntentionId, buyIntentionId);
            _marketServiceClient.Close();
            return true;
        }

    }
}