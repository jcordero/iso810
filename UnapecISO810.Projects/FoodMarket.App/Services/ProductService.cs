﻿using FoodMarket.App.AgroMarketService;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace FoodMarket.App.Services
{
    public class ProductService : InternalService
    {
      
        public ProductService(string token) : base(token)
        {
            
        }

        public Product Add(Product item)
        {
            return null;
        }

        public IEnumerable<Product> GetProducts()
        {
            var response  = _marketServiceClient.GetProducts(ServiceAccess.USERNAME, Token);
            _marketServiceClient.Close();
            return response.Products;
        }

        public IEnumerable<ProductUnit> GetUnits()
        {
            var response = _marketServiceClient.GetUnitTypes(ServiceAccess.USERNAME, Token);
            _marketServiceClient.Close();
            return response.UnitTypes;
        }


    }
}