﻿using FoodMarket.App.AgroMarketService;

namespace FoodMarket.App.Services
{
    public class InternalService
    {

        protected AgroMarketServiceClient _marketServiceClient;

        protected string Token { get; private set; }

        public InternalService(string token)
        {
            Token = token;
            _marketServiceClient = new AgroMarketServiceClient(ServiceAccess.SERVICE_ENDPOINT_NAME);
        }

        ~InternalService()
        {
            _marketServiceClient.Close();
            _marketServiceClient = null;
        }
    }
}