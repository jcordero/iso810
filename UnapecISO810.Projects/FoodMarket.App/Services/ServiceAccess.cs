﻿using System;

namespace FoodMarket.App.Services
{
    public static class ServiceAccess
    {

        public const int GROUP_ACCESS_ID = 3;

        public const string USERNAME = "equipo3";
        public const string PASSWORD = "installgentoo1";

        public const string SERVICE_ENDPOINT_NAME = "BasicHttpBinding_IAgroMarketService";


        public static string GetSessionToken()
        {
            using (var service = new AgroMarketService.AgroMarketServiceClient(SERVICE_ENDPOINT_NAME))
            {
                var response = service.SignIn(USERNAME, PASSWORD);
                service.Close();
                return response.Token;
            }
        }


    }
}