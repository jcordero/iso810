﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.IO;
using System.Linq;
using System.Net;
using System.Text;
using System.Web;

namespace FoodMarket.App.Services
{
    public class FtpManager
    {
        private FtpWebRequest ftpRequester;


        public void Upload(string content, string fileName)
        {

            ftpRequester = (FtpWebRequest)WebRequest.Create(GetHost());
            ftpRequester.Method = WebRequestMethods.Ftp.UploadFile;
            ftpRequester.UseBinary = false;
            ftpRequester.UsePassive = true;
            ftpRequester.KeepAlive = true;

            ftpRequester.Credentials = GetFtpCredentials();

            // Copy the contents of the file to the request stream.  
            StreamReader sourceStream = new StreamReader("testfile.txt");
            byte[] fileContents = Encoding.UTF8.GetBytes(content);
            sourceStream.Close();
            ftpRequester.ContentLength = fileContents.Length;

            Stream requestStream = ftpRequester.GetRequestStream();
            requestStream.Write(fileContents, 0, fileContents.Length);
            requestStream.Close();

            FtpWebResponse response = (FtpWebResponse)ftpRequester.GetResponse();

            Console.WriteLine("Upload File Complete, status {0}", response.StatusDescription);

            response.Close();
        }

        private string GetHost()
        {
            return "ftp://" + ConfigurationManager.AppSettings["FtpHost"];
        }

        private NetworkCredential GetFtpCredentials()
        {
            return new NetworkCredential(ConfigurationManager.AppSettings["FtpUser"].Normalize(), ConfigurationManager.AppSettings["FtpPassword"].Normalize());
        }
    }
}