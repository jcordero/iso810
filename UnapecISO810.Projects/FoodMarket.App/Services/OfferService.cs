﻿using FoodMarket.App.AgroMarketService;
using System.Collections.Generic;
using System.Linq;
using System;

namespace FoodMarket.App.Services
{
    public class OfferService : InternalService
    {

        public OfferService(string token) : base(token)
        {

        }

        public IEnumerable<Offer> GetOffers()
        {
            var response = _marketServiceClient.GetAllOffers(ServiceAccess.USERNAME, Token);
            _marketServiceClient.Close();
            return response.Offers;
        }

    }
}