﻿using FoodMarket.App.AgroMarketService;

namespace FoodMarket.App.Models
{
    public class IntentionBuyingProductDTO : ProductIntention
    {
        public bool IsSelected { get; set; }
    }
}