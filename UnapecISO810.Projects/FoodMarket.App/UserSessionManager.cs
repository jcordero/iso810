﻿using FoodMarket.App.Services;
using System.Web;
using System.Web.SessionState;

namespace FoodMarket.App
{
    public static class UserSessionManager
    {

        public static string TransactionServiceToken(this HttpSessionState sessionState)
        {
            var token = sessionState["SERVICE_TOKEN"] + "";

            if(string.IsNullOrEmpty(token))
            {
                token = ServiceAccess.GetSessionToken();
                sessionState.Add("SERVICE_TOKEN", token);
            }
            return token;
        }

        public static string TransactionServiceToken(this HttpSessionStateBase sessionState)
        {
            var token = sessionState["SERVICE_TOKEN"] + "";

            if (string.IsNullOrEmpty(token))
            {
                token = ServiceAccess.GetSessionToken();
                sessionState.Add("SERVICE_TOKEN", token);
            }
            return token;
        }

    }
}