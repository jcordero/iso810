﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace TextFileReaderApp
{
    public partial class InventoryForm : Form
    {

        //ID Producto, Descripcion, Costo, Precio, Existencia

        public InventoryForm()
        {
            InitializeComponent();
        }

        private void btnUpdate_Click(object sender, EventArgs e)
        {
            dataGridView1.DataSource = MainForm.Data;
            dataGridView1.Refresh();
        }

        private void btnExit_Click(object sender, EventArgs e)
        {
            this.Close();
        }

        private void InventoryForm_Load(object sender, EventArgs e)
        {
            dataGridView1.DataSource = MainForm.Data;
        }
    }
}
