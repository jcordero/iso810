﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using TextFileReaderApp.DataSource;

namespace TextFileReaderApp.Data
{
    public class RecurrentEngine
    {

        private const string PROCESSED_FILES_EXTENSION = "_PRC.txt";

        private bool _isRunning;

        private string _sourceDirectory;

        private Task _runningTask;
        private Action<IEnumerable<DataInput>> _whenCompletedTask = null;
        private CancellationToken _cancelToken;

        public RecurrentEngine()
        {
            _cancelToken = new CancellationToken();
            _runningTask = new Task(DoWork, _cancelToken);
        }

        public void Start(string path, Action<IEnumerable<DataInput>> completedAction)
        {
            _isRunning = true;
            _sourceDirectory = path;
            _whenCompletedTask = completedAction;
            _runningTask.Start();
        }

        public void Stop()
        {
            _isRunning = false;

        }

        private async void DoWork()
        {
            while (!_cancelToken.IsCancellationRequested && _isRunning)
            {
                var files = Directory.EnumerateFiles(_sourceDirectory, "*.txt", SearchOption.TopDirectoryOnly).Where(p => !p.EndsWith(PROCESSED_FILES_EXTENSION));

                IEnumerable<DataSource.DataInput> dataSource = null;

                foreach (var fileName in files)
                {
                    //Open file

                    var dataToProcess = File.ReadAllLines(fileName);

                    var tempData = new List<DataSource.DataInput>();
                    DataSource.DataInput tempLine;
                    foreach (var item in dataToProcess)
                    {

                        var parts = item.Split(',');

                        tempLine = new DataSource.DataInput
                        {
                            ProductId = parts[0],
                            Name = parts[1],
                            Cost = Convert.ToSingle(parts[2]),
                            Stock = Convert.ToInt32(parts[3]),
                            SalesQty = Convert.ToInt32(parts[4]),
                        };

                        tempData.Add(tempLine);
                    }

                    dataSource = tempData;

                    var newFileName = string.Format("{1}{0}{2}{3}", Path.DirectorySeparatorChar, Path.GetDirectoryName(fileName), Path.GetFileNameWithoutExtension(fileName), PROCESSED_FILES_EXTENSION);
                    File.Move(fileName, newFileName);
                }


                await Task.Delay(5 * 1000);

                _whenCompletedTask.Invoke(dataSource);

                try
                {
                    _cancelToken.ThrowIfCancellationRequested();
                }
                catch
                {
                    _isRunning = false;
                }
            }

        }


    }
}
