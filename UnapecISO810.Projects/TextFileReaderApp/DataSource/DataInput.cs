﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace TextFileReaderApp.DataSource
{
    public class DataInput
    {
        public string ProductId { get; set; }
        public string Name { get; set; }
        public float Cost { get; set; }
        public int Stock { get; set; }
        public int SalesQty { get; set; }
    }
}
