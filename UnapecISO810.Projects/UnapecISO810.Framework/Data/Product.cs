﻿using System.ComponentModel.DataAnnotations.Schema;

namespace UnapecISO810.Framework.Data
{
    public class Product
    {
        [DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        public int Id { get; set; }
        public string Name { get; set; }
        public float Cost { get; set; }
        public float Price { get; set; }
    }
}
