﻿using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Data.Entity.Infrastructure;

namespace UnapecISO810.Framework.Data
{
    public class DataContext : DbContext
    {

        public DataContext() : base("UnapecSqlConnection")
        {
            var creator = new CustomDatabaseCreator();
            creator.InitializeDatabase(this);
        }

        public void SecureSaveChanges()
        {
            try
            {
                base.SaveChanges();
            }
            catch (System.Data.Entity.Validation.DbEntityValidationException ex)
            {
                foreach (var error in ex.EntityValidationErrors)
                {
                    foreach (var inError in error.ValidationErrors)
                    {
                        throw new Exception(inError.ErrorMessage);
                    }
                }
            }
            catch (DbUpdateException ex)
            {
                foreach (var item in ex.Entries)
                {
                    throw new Exception(ex.Message + Environment.NewLine + item.Entity, ex.InnerException);
                }
                throw new Exception(ex.Message + Environment.NewLine + ex.InnerException.Message);
            }
            catch
            {
                throw;
            }
        }

        public DbSet<Product> Products { get; set; }
        public DbSet<Inventory> Inventories { get; set; }
        public DbSet<SaleOrder> SaleOrders { get; set; }
        public DbSet<RefillmentOrder> RefillmentOrders { get; set; }

    }

    public class CustomDatabaseCreator : System.Data.Entity.CreateDatabaseIfNotExists<DataContext>
    {

        public override void InitializeDatabase(DataContext context)
        {
            base.InitializeDatabase(context);
        }

        protected override void Seed(DataContext context)
        {
            context.Products.AddRange(new List<Product>
            {
                new Product { Name = "Leche", Cost = 31.60f, Price = 35 },
                new Product { Name = "Arroz", Cost = 6.69f, Price = 9f },
                new Product { Name = "Maizena", Cost = 1.99f, Price = 5 },
                new Product { Name = "Café Soluble", Cost = 21.99f, Price = 30 },
                new Product { Name = "Frijol", Cost = 14.00f, Price = 20 },
                new Product { Name = "Sopa", Cost =1.69f, Price = 4.95f },
                new Product { Name = "Huevos", Cost = 10.40f, Price = 20 },
                new Product { Name = "Harina de Trigo", Cost = 6.19f, Price = 9.95f },
                new Product { Name = "Azúcar", Cost = 18.40f, Price = 22.50f },
                new Product { Name = "Aceite", Cost = 7.75f, Price = 9.95f }
            });
            context.SaveChanges();

            context.Inventories.AddRange(new List<Inventory>
            {
                new Inventory { ProductId = 1, Quantity = 500, RefillPoint = 200 },
                new Inventory { ProductId = 2, Quantity = 500, RefillPoint = 100 },
                new Inventory { ProductId = 3, Quantity = 500, RefillPoint = 100 },
                new Inventory { ProductId = 4, Quantity = 500, RefillPoint = 200 },
                new Inventory { ProductId = 5, Quantity = 500, RefillPoint = 200 },
                new Inventory { ProductId = 6, Quantity = 500, RefillPoint = 200 },
                new Inventory { ProductId = 7, Quantity = 500, RefillPoint = 100 },
                new Inventory { ProductId = 8, Quantity = 500, RefillPoint = 200 },
                new Inventory { ProductId = 9, Quantity = 500, RefillPoint = 200 },
                new Inventory { ProductId = 10, Quantity = 500, RefillPoint = 200 },
            });
            context.SaveChanges();

        }


    }
}
