﻿using System.ComponentModel.DataAnnotations.Schema;

namespace UnapecISO810.Framework.Data
{
    public class Inventory
    {
        [DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        public int Id { get; set; }
        public int ProductId { get; set; }
        public int Quantity { get; set; }
        public int RefillPoint { get; set; }

        public virtual Product Product { get; set; }
    }
}
