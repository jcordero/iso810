﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;

namespace UnapecISO810.Framework.Data
{
    public class SaleOrder
    {
        [DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        public int Id { get; set; }
        public decimal Amount { get; set; }
        public DateTimeOffset RegisterDate { get; set; }
        public int ProductId { get; set; }
        public int Quantity { get; set; }
    }


}
