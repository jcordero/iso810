﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;

namespace UnapecISO810.Framework.Data
{
    public class RefillmentOrder
    {
        [DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        public int Id { get; set; }
        public int SellerId { get; set; }
        public string TransactionCode { get; set; }
        public DateTimeOffset RegisterDate { get; set; }
        public DateTimeOffset? ResponseDate { get; set; }
        public int ProductId { get; set; }
        public string ProductName { get; set; }
        public int Quantity { get; set; }
        public int QuantitySupplied { get; set; }
    }


}
