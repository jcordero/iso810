﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace UnapecISO810.Framework.Helpers
{
    public static class FormatHelper
    {

        public static bool IsBeforeOrEqual(this DateTimeOffset value, DateTimeOffset date)
        {
            return DateTimeOffset.Compare(value, date) <= 0;
        }

        public static bool IsAfterOrEqual(this DateTimeOffset value, DateTimeOffset date)
        {
            return DateTimeOffset.Compare(value, date) >= 0;
        }

        public static bool IsBetween(this DateTimeOffset value, DateTimeOffset first_date, DateTimeOffset second_date)
        {

            return value.IsAfterOrEqual(first_date) && value.IsBeforeOrEqual(second_date);
        }


    }
}
