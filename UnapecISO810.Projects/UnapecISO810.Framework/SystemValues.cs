﻿namespace UnapecISO810.Framework.Data
{
    public class SystemValues
    {

        public const int CREATOR_GROUP_ID = 3;

        public const string CREATOR_GROUP_TYPE = "Supplier";

        public const int INTERCHANGE_DESTINY_GROUP_ID = 1;

    }
}
