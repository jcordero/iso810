﻿namespace UnapecISO810.Framework.Services
{
    public class InventoryUpdateRegistry
    {
        public int ProductId { get; set; }
        public string ProductName { get; set; }
        public int SalesQuantity { get; set; }
    }
}
