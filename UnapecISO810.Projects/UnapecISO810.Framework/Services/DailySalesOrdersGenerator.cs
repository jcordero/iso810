﻿using System;
using System.Collections.Generic;
using System.Linq;
using UnapecISO810.Framework.Data;
using UnapecISO810.Framework.Helpers;

namespace UnapecISO810.Framework.Services
{
    public class DailySalesOrdersGenerator
    {

        private readonly DataContext dbContext;

        public DailySalesOrdersGenerator()
        {
            dbContext = new DataContext();
        }

        public IEnumerable<InventoryUpdateRegistry> Merge()
        {
            var now = DateTimeOffset.Now;
            var today = new DateTimeOffset(new DateTime(now.Year, now.Month, now.Day));
            var salesOrders = dbContext.SaleOrders.ToList().Where(p => p.RegisterDate.IsAfterOrEqual(today));

            var products = dbContext.Products.ToList();

            var result = new List<InventoryUpdateRegistry>();

            foreach (var item in products)
            {
                var salesQty = salesOrders.Where(p => p.ProductId == item.Id).Sum(p => p.Quantity);
                result.Add(new InventoryUpdateRegistry
                {
                    ProductId = item.Id,
                    ProductName = item.Name,
                    SalesQuantity = salesQty
                });
            }

            return result;
        }

    }
}
