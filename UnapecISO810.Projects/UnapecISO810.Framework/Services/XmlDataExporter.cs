﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Xml.Linq;

namespace UnapecISO810.Framework.Services
{
    public class XmlDataExporter : IDisposable
    {

        private XDocument _document;

        public void Dispose()
        {
            _document = null;
        }

        public IEnumerable<InventoryUpdateRegistry> Read(string content)
        {
            _document = XDocument.Parse(content);
            var result = _document.Element(MAIN_XML_ELEMENT_NAME).Elements(XML_ELEMENT_NAME).Select(ToEntity);
            return result;
        }

        public string Serialize(IEnumerable<InventoryUpdateRegistry> entities)
        {
            var elements = entities.Select(ToElement);

            _document = new XDocument();
            _document.Add(new XElement(MAIN_XML_ELEMENT_NAME, elements));
            return _document.ToString(SaveOptions.None);
        }

        private InventoryUpdateRegistry ToEntity(XElement item)
        {
            var entity = new InventoryUpdateRegistry
            {
                ProductId = Convert.ToInt32(item.Attribute(PRODUCT_ID_ELEMENT).Value),
                ProductName = item.Element(PRODUCT_NAME_ELEMENT).Value,
                SalesQuantity = Convert.ToInt32(item.Element(SALES_QUANTITY_ELEMENT).Value)
            };
            return entity;
        }

        private XElement ToElement(InventoryUpdateRegistry entity)
        {
            var item = new XElement(XML_ELEMENT_NAME, 
                new XAttribute(PRODUCT_ID_ELEMENT, entity.ProductId),
                new XElement(PRODUCT_NAME_ELEMENT, entity.ProductName),
                new XElement(SALES_QUANTITY_ELEMENT, entity.SalesQuantity));
            return item;
        }



        private const string MAIN_XML_ELEMENT_NAME = "records";
        private const string XML_ELEMENT_NAME = "registry";
        private const string PRODUCT_ID_ELEMENT = "pId";
        private const string PRODUCT_NAME_ELEMENT = "name";
        private const string SALES_QUANTITY_ELEMENT = "sqty";

    }
}
