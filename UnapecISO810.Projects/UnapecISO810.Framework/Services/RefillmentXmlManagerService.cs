﻿
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Xml.Linq;
using UnapecISO810.Framework.Data;

namespace UnapecISO810.Framework.Services
{
    public class RefillmentXmlManagerService : IDisposable
    {

        private XDocument _document;

        public void Dispose()
        {
            _document = null;
        }

        public IEnumerable<RefillmentOrder> GetRequest(string fileName)
        {
            var content = File.ReadAllText(fileName);

            _document = XDocument.Parse(content, LoadOptions.None);

            // destiny Validation
            var sellerId = _document.Element(RequestXmlElements.MAIN_XML_ELEMENT_NAME)
                                .Element(RequestXmlElements.MAIN_LEVEL_SELLER_ELEMENT).Value;

            sellerId = System.Text.RegularExpressions.Regex.Replace(sellerId, "[^0-9]", "");

            if (sellerId != SystemValues.INTERCHANGE_DESTINY_GROUP_ID.ToString())
                return null;


            var result = new List<RefillmentOrder>();

            foreach (var node in _document.Element(RequestXmlElements.MAIN_XML_ELEMENT_NAME)
                                    .Element(RequestXmlElements.MAIN_XML_SECOND_LEVEL_ELEMENT_NAME)
                                    .Elements(RequestXmlElements.XML_ELEMENT_NAME))
            {
                var entity = RequestXmlElements.ToEntity(node);

                var refillmentOrder = new RefillmentOrder
                {
                    ProductId = entity.ProductId,
                    ProductName = entity.ProductName,
                    Quantity = entity.SalesQuantity,
                    RegisterDate = DateTimeOffset.Now,
                };
                var fileParts = Path.GetFileNameWithoutExtension(fileName).Split('_');

                refillmentOrder.SellerId = Convert.ToInt32(sellerId);
                refillmentOrder.TransactionCode = Guid.NewGuid().ToString().Split('-').First();// DateTimeOffset.Now.Ticks.ToString(), fileParts[2]; //

                result.Add(refillmentOrder);
            }

            return result;
        }

        public string CreateResponse(RefillmentOrder request)
        {
            _document = new XDocument();

            var secondLevelNode = new XElement(ResponseXmlElements.MAIN_XML_SECOND_LEVEL_ELEMENT_NAME, ResponseXmlElements.ToElement(request));

            _document.Add(new XElement(ResponseXmlElements.MAIN_XML_ELEMENT_NAME, 
                            new XElement(ResponseXmlElements.MAIN_LEVEL_DATE_ELEMENT, DateTimeOffset.Now.ToString("yyyyMMddhhmmss")),
                            new XElement(ResponseXmlElements.MAIN_LEVEL_SUPPLIER_ELEMENT, SystemValues.CREATOR_GROUP_ID),
                            new XElement(ResponseXmlElements.MAIN_LEVEL_SELLER_ELEMENT, SystemValues.INTERCHANGE_DESTINY_GROUP_ID),
                            secondLevelNode));
            return _document.ToString(SaveOptions.None);
        }


        public static class RequestXmlElements
        {
            public const string MAIN_XML_ELEMENT_NAME = "Solicitud";

            public const string MAIN_LEVEL_DATE_ELEMENT = "FechaDeCreacion";
            public const string MAIN_LEVEL_SUPPLIER_ELEMENT = "Suplidor";
            public const string MAIN_LEVEL_SELLER_ELEMENT = "Vendedor";

            public const string MAIN_XML_SECOND_LEVEL_ELEMENT_NAME = "Productos";

            public const string XML_ELEMENT_NAME = "Producto";
            public const string PRODUCT_ID_ELEMENT = "ProductoId";
            public const string PRODUCT_NAME_ELEMENT = "Descripcion";
            public const string SALES_QUANTITY_ELEMENT = "Cantidad";

            public static InventoryUpdateRegistry ToEntity(XElement item)
            {
                var entity = new InventoryUpdateRegistry
                {
                    ProductId = Convert.ToInt32(item.Element(PRODUCT_ID_ELEMENT).Value),
                    ProductName = item.Element(PRODUCT_NAME_ELEMENT).Value,
                    SalesQuantity = Convert.ToInt32(item.Element(SALES_QUANTITY_ELEMENT).Value)
                };
                return entity;
            }
        }

        public static class ResponseXmlElements
        {
            public const string MAIN_XML_ELEMENT_NAME = "Entrega";

            public const string MAIN_LEVEL_DATE_ELEMENT = "FechaDeCreacion";
            public const string MAIN_LEVEL_SUPPLIER_ELEMENT = "Suplidor";
            public const string MAIN_LEVEL_SELLER_ELEMENT = "Vendedor";

            public const string MAIN_XML_SECOND_LEVEL_ELEMENT_NAME = "Productos";

            public const string XML_ELEMENT_NAME = "Producto";
            public const string PRODUCT_ID_ELEMENT = "ProductoId";
            public const string PRODUCT_NAME_ELEMENT = "Descripcion";
            public const string SALES_QUANTITY_ELEMENT = "Cantidad";

            public static XElement ToElement(RefillmentOrder entity)
            {
                var item = new XElement(XML_ELEMENT_NAME,
                    new XElement(PRODUCT_ID_ELEMENT, entity.ProductId),
                    new XElement(PRODUCT_NAME_ELEMENT, entity.ProductName),
                    // Quantity != QuantitySupplied
                    new XElement(SALES_QUANTITY_ELEMENT, entity.QuantitySupplied));
                return item;
            }
        }

    }
}
