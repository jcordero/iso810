﻿using System;
using System.Collections.Generic;
using System.Data.Entity.Migrations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using UnapecISO810.Framework.Data;

namespace UnapecISO810.Framework.Services
{
    public class SalesOrderService
    {

        private readonly DataContext dbContext;
        private readonly RefillmentXmlManagerService _refillmentXmlGeneratorService;

        public SalesOrderService()
        {
            dbContext = new DataContext();
            _refillmentXmlGeneratorService = new RefillmentXmlManagerService();
        }


        public void RegisterSales(SaleOrder sale)
        {
            dbContext.SaleOrders.Add(sale);

            var inventory = dbContext.Inventories.Single(p => p.ProductId == sale.ProductId);
            inventory.Quantity -= sale.Quantity;
            dbContext.Inventories.AddOrUpdate(inventory);

            dbContext.SecureSaveChanges();
            //
            if (inventory.Quantity <= inventory.RefillPoint)
            {
                var request = new RefillmentOrder
                {
                    ProductId = inventory.Product.Id,
                    ProductName = inventory.Product.Name,
                    Quantity = inventory.RefillPoint * 3,
                    SellerId = 3,
                    RegisterDate = DateTimeOffset.Now,
                    TransactionCode = Guid.NewGuid().ToString()
                };
                dbContext.RefillmentOrders.Add(request);
                dbContext.SecureSaveChanges();

                //_refillmentXmlGeneratorService.CreateRequest(request);
            }

        }

    }
}
