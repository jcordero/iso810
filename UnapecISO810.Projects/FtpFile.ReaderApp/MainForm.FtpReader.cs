﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Configuration;
using System.IO;
using System.Linq;
using System.Net;
using System.Text;
using System.Threading.Tasks;

namespace FtpFile.ReaderApp
{

    public partial class MainForm
    {

        private FtpWebRequest ftpRequester;

        private IEnumerable<KeyValuePair<string, string>> _ftpResponseResult;


        private void backgroundWorker1_DoWork(object sender, DoWorkEventArgs e)
        {

            ftpRequester = (FtpWebRequest)WebRequest.Create(GetHost());
            ftpRequester.Method = WebRequestMethods.Ftp.ListDirectory;
            ftpRequester.UseBinary = false;
            ftpRequester.UsePassive = true;
            ftpRequester.KeepAlive = true;

            ftpRequester.Credentials = GetFtpCredentials();

            FtpWebResponse response = (FtpWebResponse)ftpRequester.GetResponse();
            Stream responseStream = response.GetResponseStream();
            StreamReader reader = new StreamReader(responseStream);

            var result = new List<KeyValuePair<string, string>>();

            string line = reader.ReadLine();
            while (!string.IsNullOrEmpty(line))
            {
                if(!line.StartsWith("."))
                {
                    string content = GetContent(line);
                    result.Add(new KeyValuePair<string, string>(line, content));
                }
                line = reader.ReadLine();
            }
            _ftpResponseResult = result;

            reader.Close();
            response.Close();

        }

        private void backgroundWorker1_RunWorkerCompleted(object sender, RunWorkerCompletedEventArgs e)
        {
            if (!e.Cancelled)
                UpdateLeftSidePanel();
        }

        private string GetHost()
        {
            return "ftp://" + ConfigurationManager.AppSettings["FtpHost"];
        }

        private NetworkCredential GetFtpCredentials()
        {
            return new NetworkCredential(ConfigurationManager.AppSettings["FtpUser"].Normalize(), ConfigurationManager.AppSettings["FtpPassword"].Normalize());
        }

        private string GetContent(string path)
        {
            var request = (FtpWebRequest)WebRequest.Create(string.Format("{0}/{1}", GetHost(), path));
            request.Method = WebRequestMethods.Ftp.DownloadFile;

            request.UseBinary = false;
            request.UsePassive = true;
            request.KeepAlive = true;

            request.Credentials = GetFtpCredentials();

            FtpWebResponse response = (FtpWebResponse)request.GetResponse();
            Stream responseStream = response.GetResponseStream();
            StreamReader reader = new StreamReader(responseStream);

            string result = reader.ReadToEnd();
            reader.Close();
            response.Close();

            return result;
        }

    }
}
