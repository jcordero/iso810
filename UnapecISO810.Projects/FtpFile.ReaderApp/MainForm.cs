﻿using System;
using System.Windows.Forms;

namespace FtpFile.ReaderApp
{
    public partial class MainForm : Form
    {

        private bool _isRunning;

        public MainForm()
        {
            InitializeComponent();
        }

        private void iniciarToolStripMenuItem_Click(object sender, EventArgs e)
        {
            _isRunning = true;

            iniciarToolStripMenuItem.Enabled = !_isRunning;
            detenerToolStripMenuItem.Enabled = _isRunning;
            lblStatus.Text = String.Format("Iniciado: {0}", DateTimeOffset.Now);
           
            backgroundWorker1.RunWorkerAsync();
        }

        private void detenerToolStripMenuItem_Click(object sender, EventArgs e)
        {
            _isRunning = false;
            backgroundWorker1.CancelAsync();

            lblStatus.Text = String.Format("Detenido: {0}", DateTimeOffset.Now);
            iniciarToolStripMenuItem.Enabled = true;
            detenerToolStripMenuItem.Enabled = false;
        }

        private void salirToolStripMenuItem_Click(object sender, EventArgs e)
        {
            Application.Exit();
        }

        private void listView1_SelectedIndexChanged(object sender, EventArgs e)
        {
            if (listView1.SelectedIndices.Count == 0) return;

            var itemPosition = listView1.SelectedIndices[0];
            txtSelectedFile.Text = listView1.Items[itemPosition].Tag.ToString();
        }

        private void UpdateLeftSidePanel()
        {
            iniciarToolStripMenuItem.Enabled = true;
            detenerToolStripMenuItem.Enabled = false;

            listView1.Clear();
            foreach (var item in _ftpResponseResult)
            {
                listView1.Items.Add(new ListViewItem(item.Key) { Tag = item.Value });
            }
        }
    }
}
