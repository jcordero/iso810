﻿using System;
using System.Windows.Forms;
using UnapecISO810.Framework.Data;
using UnapecISO810.Framework.Services;

namespace XmlFile.WriterApp
{
    public partial class FormMain : Form
    {

        public FormMain()
        {
            InitializeComponent();

        }

        private void salirToolStripMenuItem_Click(object sender, EventArgs e)
        {
            Application.Exit();
        }

        private void exportarInventarioToolStripMenuItem_Click(object sender, EventArgs e)
        {
            var r = saveFileDialog1.ShowDialog(this);

            if (r != DialogResult.OK)
                return;

            var salesOrdersGenerator = new DailySalesOrdersGenerator();
            var todayInventory = salesOrdersGenerator.Merge();

            using (var exporter = new XmlDataExporter())
            {
                var content = exporter.Serialize(todayInventory);
                System.IO.File.WriteAllText(saveFileDialog1.FileName, content);
            }

            MessageBox.Show(this, "Archivo generado exitosamente.");
        }

        private void registrarVentaToolStripMenuItem_Click(object sender, EventArgs e)
        {
            var frm = new FormSales();
            frm.MdiParent = this;
            frm.Show();
        }

        private void productosToolStripMenuItem_Click(object sender, EventArgs e)
        {
            var frm = new FormProducts();
            frm.MdiParent = this;
            frm.Show();
        }

        private void totalDeVentasToolStripMenuItem_Click(object sender, EventArgs e)
        {
            var frm = new FrmTotalSales();
            frm.MdiParent = this;
            frm.Show();
        }
    }
}
