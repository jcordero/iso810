﻿using System;
using System.Linq;
using System.Windows.Forms;
using UnapecISO810.Framework.Data;
using UnapecISO810.Framework.Helpers;

namespace XmlFile.WriterApp
{
    public partial class FrmTotalSales : Form
    {

        private readonly DataContext dbContext;

        public FrmTotalSales()
        {
            InitializeComponent();

            dbContext = new DataContext();
        }

        private void FrmTotalSales_Load(object sender, EventArgs e)
        {
            var now = DateTimeOffset.Now;
            var today = new DateTimeOffset(new DateTime(now.Year, now.Month, now.Day));
            var salesOrders = dbContext.SaleOrders.ToList();

            dataGridView1.DataSource = salesOrders;
        }
    }
}
