﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using UnapecISO810.Framework.Data;

namespace XmlFile.WriterApp
{
    public partial class FormProducts : Form
    {

        private readonly DataContext dbContext;

        public FormProducts()
        {
            InitializeComponent();

            dbContext = new DataContext();

        }

        private void FormProducts_Load(object sender, EventArgs e)
        {
            dataGridView1.DataSource = dbContext.Products.ToList();
        }
    }
}
