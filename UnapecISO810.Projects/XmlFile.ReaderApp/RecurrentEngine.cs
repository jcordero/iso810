﻿using System;
using System.Collections.Generic;
using System.Data.Entity.Migrations;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using UnapecISO810.Framework.Data;
using UnapecISO810.Framework.Services;

namespace XmlFile.ReaderApp
{
    public class RecurrentEngine
    {

        private const string PROCESSED_FILES_EXTENSION = "_PRC.xml";

        private bool _isRunning;

        private string _sourceDirectory;

        private Task _runningTask;
        private Action<IEnumerable<InventoryUpdateRegistry>> _whenCompletedTask = null;
        private CancellationToken _cancelToken;
        private XmlDataExporter xmlExporter;

        public RecurrentEngine()
        {
            _cancelToken = new CancellationToken();
            _runningTask = new Task(DoWork, _cancelToken);
            xmlExporter = new XmlDataExporter();
        }

        public void Start(string path, Action<IEnumerable<InventoryUpdateRegistry>> completedAction)
        {
            _isRunning = true;
            _sourceDirectory = path;
            _whenCompletedTask = completedAction;
            _runningTask.Start();
        }

        public void Stop()
        {
            _isRunning = false;

        }

        private async void DoWork()
        {
            while (!_cancelToken.IsCancellationRequested && _isRunning)
            {
                var files = Directory.EnumerateFiles(_sourceDirectory, "*.xml", SearchOption.TopDirectoryOnly).Where(p => !p.EndsWith(PROCESSED_FILES_EXTENSION));
                IEnumerable<InventoryUpdateRegistry> dataSource = null;

                foreach (var fileName in files)
                {
                    //Open file
                    var dataToProcess = File.ReadAllText(fileName);

                    dataSource = xmlExporter.Read(dataToProcess);

                    using (var dbContext = new DataContext())
                    {
                        foreach (var product in dataSource)
                        {
                            if (product.SalesQuantity <= 0)
                                continue;

                            var inventory = dbContext.Inventories.Single(p => p.ProductId == product.ProductId);
                            inventory.Quantity -= product.SalesQuantity;

                            dbContext.Inventories.AddOrUpdate(inventory);
                        }
                        dbContext.SecureSaveChanges();
                    }

                    var newFileName = string.Format("{1}{0}{2}{3}", Path.DirectorySeparatorChar, Path.GetDirectoryName(fileName), Path.GetFileNameWithoutExtension(fileName), PROCESSED_FILES_EXTENSION);
                    File.Move(fileName, newFileName);
                }

                await Task.Delay(5 * 1000);

                _whenCompletedTask.Invoke(dataSource);
                try
                {
                    _cancelToken.ThrowIfCancellationRequested();
                }
                catch
                {
                    _isRunning = false;
                }
            }

        }


    }
}
