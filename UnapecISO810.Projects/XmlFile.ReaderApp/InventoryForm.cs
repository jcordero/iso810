﻿using System;
using System.Data;
using System.Linq;
using System.Windows.Forms;
using UnapecISO810.Framework.Data;

namespace XmlFile.ReaderApp
{
    public partial class InventoryForm : Form
    {

        private readonly DataContext dbContext;

        public InventoryForm()
        {
            InitializeComponent();
            dbContext = new DataContext();
        }

        private void btnUpdate_Click(object sender, EventArgs e)
        {
            LoadData();
        }

        private void btnExit_Click(object sender, EventArgs e)
        {
            this.Close();
        }

        private void InventoryForm_Load(object sender, EventArgs e)
        {
            LoadData();
        }

        private void LoadData()
        {
            dataGridView1.DataSource = dbContext.Inventories.Select(p =>
                                          new
                                          {
                                              Producto = p.Product.Name,
                                              Precio = p.Product.Price,
                                              Inventario = p.Quantity
                                          }).ToList();
            dataGridView1.Refresh();
        }
    }
}
