﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace XmlFile.ReaderApp
{
    public partial class MainForm : Form
    {

        RecurrentEngine _engine;


        public MainForm()
        {
            InitializeComponent();
            _engine = new RecurrentEngine();
        }

        private void iniciarToolStripMenuItem_Click(object sender, EventArgs e)
        {
            if(String.IsNullOrEmpty(textBox1.Text))
            {
                MessageBox.Show(this, "No puede iniciar el lector sin indicar la ruta de archivos.", "",  MessageBoxButtons.OK, MessageBoxIcon.Error);
                return;
            }
            iniciarToolStripMenuItem.Enabled = false;
            detenerToolStripMenuItem.Enabled = true;
            lblStatus.Text = String.Format("Iniciado: {0}", DateTimeOffset.Now);

            _engine.Start(textBox1.Text, (data) => 
            {
               //if(data != null)
               // {
               //     lock (Data)
               //     {
               //         Data = data;
               //     }
               // }
            });
        }

        private void detenerToolStripMenuItem_Click(object sender, EventArgs e)
        {
            _engine.Stop();
            lblStatus.Text = String.Format("Detenido: {0}", DateTimeOffset.Now);
            iniciarToolStripMenuItem.Enabled = true;
            detenerToolStripMenuItem.Enabled = false;
        }

        private void salirToolStripMenuItem_Click(object sender, EventArgs e)
        {
            Application.Exit();
        }

        private void btnUpdatePath_Click(object sender, EventArgs e)
        {
            var r = folderBrowserDialog1.ShowDialog(this);

            if (r != DialogResult.OK) return;

            textBox1.Text = folderBrowserDialog1.SelectedPath;
        }

        private void inventarioToolStripMenuItem_Click(object sender, EventArgs e)
        {
            var frm = new InventoryForm();
            frm.Show();
        }
    }
}
